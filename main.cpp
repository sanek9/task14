#include <windows.h> // заголовочный файл, содержащий WINAPI
#include <iostream>
#include <fstream>
#include <string>
#include <locale>
#include <time.h>

using namespace std;
// Прототип функции обработки сообщений с пользовательским названием:
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
ATOM createMainWindowClass(HINSTANCE, WNDPROC, LPCTSTR);
HWND hMainWnd;
HINSTANCE hInst;

// Управляющая функция:
DWORD WINAPI ThreadFunc(LPVOID lpParam){
	while(1){
		SendMessage(hMainWnd, WM_TIMER, 0, 0);
		Sleep(20);
	}
	return 0;
}

int WINAPI WinMain(HINSTANCE hInst, // дескриптор экземпляра приложения
                   HINSTANCE hPrevInst, // не используем
                   LPSTR lpCmdLine, // не используем
                   int nCmdShow) // режим отображения окошка
{
	::hInst = hInst;
	TCHAR szClassName[] = "WinClass";
	srand(time(NULL));

    if(!createMainWindowClass(hInst, WndProc, szClassName))
		return -1;
    hMainWnd = CreateWindow(
        szClassName, "Program (V2)", WS_OVERLAPPEDWINDOW,
	//	WS_BORDER|WS_CAPTION, // режимы отображения окошка
        CW_USEDEFAULT, CW_USEDEFAULT, 400, 400,
        (HWND)NULL, NULL, HINSTANCE(hInst), NULL);
    if(!hMainWnd){
        return NULL;
    }
	ShowWindow(hMainWnd, nCmdShow);
    UpdateWindow(hMainWnd); // обновляем окошко
	MSG msg;
    while(GetMessage(&msg, NULL, NULL, NULL)){
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    return msg.wParam;
}
ATOM createMainWindowClass(HINSTANCE hInst, WNDPROC WndProc, LPCTSTR szClassName){
	WNDCLASSEX wc; // создаём экземпляр, для обращения к членам класса WNDCLASSEX
    wc.cbSize        = sizeof(wc); // размер структуры (в байтах)
    wc.style         = CS_HREDRAW | CS_VREDRAW; // стиль класса окошка
    wc.lpfnWndProc   = WndProc; // указатель на пользовательскую функцию
    wc.lpszMenuName  = NULL; // указатель на имя меню (у нас его нет)
    wc.lpszClassName = szClassName; // указатель на имя класса
    wc.cbWndExtra    = NULL; // число освобождаемых байтов в конце структуры
    wc.cbClsExtra    = NULL; // число освобождаемых байтов при создании экземпляра приложения
    wc.hIcon         = LoadIcon(NULL, IDI_WINLOGO); // декриптор пиктограммы
    wc.hIconSm       = LoadIcon(NULL, IDI_WINLOGO); // дескриптор маленькой пиктограммы (в трэе)
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW); // дескриптор курсора
   // wc.hbrBackground = CreateSolidBrush(RGB(255, 0, 0));//(HBRUSH)(COLOR_WINDOW+1);
    wc.hbrBackground = CreateSolidBrush ( RGB ( rand()%256, rand()%256, rand()%256));//(HBRUSH)GetStockObject(WHITE_BRUSH); // дескриптор кисти для закраски фона окна
    wc.hInstance     = hInst; // указатель на строку, содержащую имя меню, применяемого для класса
	return RegisterClassEx(&wc);
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam){
	static int cx = 1;
	static int width = 1;
	static int height = 1;
	HANDLE hThread;
	HBRUSH hbrush;
	DWORD dwThreadId, dwThrdParam = 1;
	RECT rect;
	ofstream F;
	HDC hdc;
	PAINTSTRUCT ps;
	DWORD c[] = {RGB(255,0,0),RGB(0,0,255)};
    switch(uMsg){
		case WM_CREATE:
	//		hThread = CreateThread(NULL, 0, ThreadFunc, &dwThrdParam, 0, &dwThreadId);
			SetTimer(hWnd, 100, 20, NULL);
			break;
		case WM_TIMER:
		//	cout << "q"<< endl;
			if(GetWindowRect(hWnd, &rect)){
				width = rect.right - rect.left;
				height = rect.bottom - rect.top;
			}
			cx += 2;
			cx %= (width-120)*2;
			InvalidateRect(hWnd, NULL, TRUE);
			break;
		case WM_COMMAND:

		break;
	case WM_PAINT:
	//	cout << "w\n";
		hdc = BeginPaint(hWnd, &ps);
	//	hdc = GetDC(hWnd);

		hbrush = CreateSolidBrush(c[(width-120-cx>0)]);
		SelectObject(hdc, hbrush);
		Rectangle(hdc, abs(width-120-cx)+ 10, 20, abs(width-120-cx)+ 100, 120);
	//	ReleaseDC(hWnd,hdc);
		EndPaint(hWnd, &ps);
		DeleteObject(hbrush);
		break;
    case WM_DESTROY: // если окошко закрылось, то:
		KillTimer (hWnd, 100);
        PostQuitMessage(NULL); // отправляем WinMain() сообщение WM_QUIT
        break;
    default:
        return DefWindowProc(hWnd, uMsg, wParam, lParam); // если закрыли окошко
    }
    return NULL; // возвращаем значение
}
